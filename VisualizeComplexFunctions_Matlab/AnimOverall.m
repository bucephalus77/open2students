function [] = AnimOverall(y, AnimationStep, x, cmap, varargin) 
if (nargin==4)
    for anim = 0:AnimationStep:1   

        yplot = anim*y + (1-anim)*x;
        
        cla %('reset');
        scatter(yplot(1, :), yplot(2, :), 10, cmap, 'filled');
        grid on;
        
        pause(0.1)
    end
elseif (nargin == 5)
    dimdiff = length(y)-size(cmap,1);
    for anim = 0:AnimationStep:1   

        yplot = anim*y + (1-anim)*x;
        
        cla %('reset');
        scatter(yplot(1, :), yplot(2, :), 10, [cmap; zeros(dimdiff,3)], 'filled');
        grid on;
        
        pause(0.3)
    end
end
end

