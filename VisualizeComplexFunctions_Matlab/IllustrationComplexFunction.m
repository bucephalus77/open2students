clear all; close all;
%% Definitions and parameters
% w = u + iv = f(z) = f(x+iy)
danim = 7; ptime = 0;                               % animation parameters. Plot every danim sample and pause ptime between each frame
lowlim = -5; highlim = 5; dmesh = 0.1; pmul = 2;   % meshing options. low and high limits are defined, as well as the granularity of the mesh with dmesh and the plotting factor pmul (mesh is multiplied by pmul when plotted)
x = lowlim:dmesh:highlim;                           % meshing definitions
y = x;                                              % arbitrarily setting same mesh on x and y for symmetry and simplicity
[X,Y] = meshgrid(x,y);

%% User input
PlotType = 3; 
    % 1 - animate all objects point per point
    % 2 - plot object per object (much faster than option 1)
    % 3 - feed entire input space to function and animate the complete
    % transformation
DefInterestingPoints = 0;
    % 1 - define equation to plot extra interesting points on the plot on
    % line 138. Only supported with PlotType = 3;
    % 0 - ignore definition of extra points.
functionToPlot = 'else';
%     Choices are: 
%         square
%         squarePlusOne
%         cube
%         inverse
%         sqrt  
%         exp
%         cos
%         cosh
%         poly
%         else (then define it yourself at line 74 and 124)

%Parametric equations to be plotted in input space and fed to the function. Only used for PlotType 1 and 2.
ParamEqToPlot = {find(Y == 0 & X >= 0)'; ...
                 find(Y == 0 & X <= 0)'; ...
                 find(X == 0 & Y >= 0)'; ...
                 find(X == 0 & Y <= 0)'; ...
                 find(X == 1.5)'; ...  
                 find(X == 2.5)'; ... 
                 find(X == -0.5)'; ...
                 find(X == -1.5)'; ...
                 find(Y == 1)'; ...
                 find(Y == -1.5)'; ...
%                  find(Y == 0 & X >= 0)'; ...
%                  find(Y == 0 & X <= 0)'; ...
%                  find(X == 0 & Y >= 0)'; ...
%                  find(X == 0 & Y <= 0)'; ...
       };

%% Plot initialization section
figure('Name', 'Complex function Illustration', 'Units', 'normalized', 'Position', [0.01 0.01 0.95 0.95]);
sf1 = subplot(1,2,1);
plot(0,0);grid on; hold on;
axis(pmul*[lowlim highlim lowlim highlim])
title('bronruimte : z = x+iy')

sf2 = subplot(1,2,2);
plot(0,0);grid on; hold on;
axis(pmul*[lowlim highlim lowlim highlim])
title(strcat('beeldruimte : w = u+iv : ', functionToPlot))

%% Animation Section
cmap = jet(size(ParamEqToPlot,1));
if (PlotType == 1 || PlotType == 2)
for obj = 1:size(ParamEqToPlot,1)
ParamEqToPlot{obj} = sort(ParamEqToPlot{obj});   
Z = X(ParamEqToPlot{obj})+1i*Y(ParamEqToPlot{obj});
switch functionToPlot
    case 'square'
        w = Z.^2;
    case 'squarePlusOne'
        w = Z.^2+1;
    case 'cube'
        w = Z.^3;
    case 'inverse'
        w = 1./Z;
    case 'sqrt'
        w = sqrt(Z);
    case 'exp'
        w = exp(Z);
    case 'cos'
        w = cos(Z);
    case 'cosh'
        w = cosh(Z);
    case 'poly'
        w = ((Z+3-2.8i).*(Z+3+2.8i))./(Z-1);
    case 'else'
        w = 1./(Z.^2+1);
end
 
u = real(w); v = imag(w);
    pause()
    if (PlotType == 2)
        cmap = jet(size(ParamEqToPlot{obj},2));
        axes(sf1);
        scatter(X(ParamEqToPlot{obj}), Y(ParamEqToPlot{obj}),10,cmap,'filled')
        axes(sf2);
        scatter(u, v,10,cmap,'filled')
    elseif (PlotType == 1)
        for anim = 1:danim:length(ParamEqToPlot{obj})
            axes(sf1);
            plot(X(ParamEqToPlot{obj}(anim)), Y(ParamEqToPlot{obj}(anim)), 'bo','MarkerSize',10,'MarkerEdgeColor','b','MarkerFaceColor', cmap(obj,:))
            %pause(ptime)
        end
        pause()
        for anim = 1:danim:length(ParamEqToPlot{obj})
            axes(sf2);
            plot(u(anim), v(anim), 'bo','MarkerSize',10,'MarkerEdgeColor','b','MarkerFaceColor',cmap(obj,:))
            %pause(ptime)
        end
    else
        
    end
end
elseif (PlotType == 3) 
    Xr = reshape(X,size(X,1)*size(X,2),1); Yr = reshape(Y,size(Y,1)*size(Y,2),1);
    [~, indColDist] = sort(sqrt(Xr.^2 + Yr.^2));
    Xr = Xr(indColDist); Yr = Yr(indColDist); Z = (Xr+1i*Yr);
    cmap = jet(length(Xr));
    switch functionToPlot
        case 'square'
            Wr = Z.^2;
        case 'squarePlusOne'
            Wr = Z.^2+1;
        case 'cube'
            Wr = Z.^3;
        case 'inverse'
            Wr = 1./Z;
        case 'sqrt'
            Wr = sqrt(Z);    
        case 'exp'
            Wr = exp(Z);
        case 'cos'
            Wr = cos(Z);
        case 'cosh'
            Wr = cosh(Z);
        case 'poly'
            Wr = ((Z+3-2.8i).*(Z+3+2.8i))./(Z-1);
        case 'else'
            Wr = 1./(Z.^2+1); %z+z0 z*i  z*exp(1i*pi/2)*2 real(z) imag(z) conj(z) zconj(z) abs(z) angle(z) 
    end
    U = real(Wr); V = imag(Wr);
    
    if DefInterestingPoints
        IPind = find(abs(Wr+5)<= 0.5*dmesh); %find some interesting points  
        Uip = U(IPind); Vip = V(IPind); Xip = Xr(IPind); Yip = Yr(IPind);
    else
        Uip = []; Vip = []; Xip = []; Yip = [];
    end
    axes(sf1);
    scatter(Xr, Yr,10,cmap,'filled')
    scatter(Xip, Yip,30,[0 0 0],'filled')
    axes(sf2);
    scatter(Xr, Yr,10,cmap,'filled')
    scatter(Xip, Yip,30,[0 0 0],'filled')
    pause()
    AnimOverall([[U' Uip'];[V' Vip']], 0.02, [[Xr' Xip']; [Yr' Yip']],cmap,1);
    scatter(Uip, Vip,30,[0 0 0],'filled')
else
end
