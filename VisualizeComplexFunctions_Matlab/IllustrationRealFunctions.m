clear all; close all; 
danim = 11; ptime = 0.05; tbeg = -5; tend = 5;
EraseFormerDots = 0;
functionToPlot = 'squarePlusOne';

t = tbeg:0.01:tend;
switch functionToPlot
    case 'squarePlusOne'
        y = t.^2 + 1;
    case 'cos'
        y = cos(t);
end

yminplot = min(y) - 2; ymaxplot = max(y) + 2; 

f1 = figure('Name', 'Real parabola Illustration', 'Units', 'normalized', 'Position', [0.01 0.01 0.95 0.95]);

sf1 = subplot(3,3,[1 4 7])
bplot = plot(t,y)
grid on
hold on;
axis([tbeg tend yminplot ymaxplot])
title('y in function of x :  y = x^2+1')

sf2 = subplot(3,3,5)
pointplot2 = plot(0,0)
grid on; hold on;
axis([tbeg tend -1 1])
title('x - independant variable')

sf3 = subplot(3,3,6)
pointplot3 = plot(0,0)
grid on; hold on;
axis([yminplot ymaxplot -1 1])
title('y - dependant variable')

cmap = jet(length(t)/danim);
for anim = 1:danim:length(t)
    
    axes(sf1)
    pointplot1 = plot(t(anim),y(anim), 'bo','MarkerSize',10,'MarkerEdgeColor','b','MarkerFaceColor',cmap(round(anim/danim)+1,:))
    
    axes(sf2)
    pointplot2 = plot(t(anim),0, 'bo','MarkerSize',10,'MarkerEdgeColor','b','MarkerFaceColor',cmap(round(anim/danim)+1,:))

    axes(sf3)
    pointplot3 = plot(y(anim),0, 'bo','MarkerSize',10,'MarkerEdgeColor','b','MarkerFaceColor',cmap(round(anim/danim)+1,:))
    
    if anim == 1
        pause()
    else
        pause(ptime)
    end
    if (anim ~= length(t) && EraseFormerDots)
        set(pointplot1,'Visible','off')
        set(pointplot2,'Visible','off')
        set(pointplot3,'Visible','off')
    end
    
    
end