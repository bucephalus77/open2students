clear all; close all; 
lim = 10;       % symmetric limit of the x axis
nlim = 2;       % new restricted limit of the x axis to avoid large computations
dt = 0.001;     % step between each point
pvec = [-10 10 -1 5]; p1 = 0.1; p2 = 0.001; % viewing window; pause time 1; pause time 2;

x = -lim:dt:lim;      % domain definition of the function
x2 = -nlim:dt:nlim;   % restricted domain to avoid complex computation (like 10^50). It turns out this is useless as Matlab handles Inf values well... 
x3_1 = -lim:dt:-nlim; % restricted domain part 1 when convergence expected from a value to inf
x3_2 = nlim:dt:lim;   % restricted domain part 2 when convergence expected from a value to inf

y = 1./(x-2);                            %function to be apprimated
yni = @(xdom, n) 1/4*((6-xdom)/4).^n;    %terms in series expansion

simu = 'LaurentNegx2';  % which simulation to be ran
%possibilities 
%   - cos
%   - TaylorFailx2
%   - LaurentNegx2
%   - TaylorFailx
%   - LaurentNegx
%   - else (then y and yni are not overwritten)
conv = 'full';          % expected convergence domain
%possibilities 
%   - BiggerThanz0
%   - SmallerThanZ0
%   - full (default)

%% Selection of simulation
switch simu
    case 'cos'
        y = cos(x);               
        yni = @(xdom, n) (-1)^(n)*xdom.^(2*n)/factorial(2*n); 
        conv = 'full'; 
    case 'TaylorFailx2'
        y = 1./(x.^2+1);
        yni = @(xdom, n) (-1)^(n)*xdom.^(2*n); 
        conv = 'full';
    case 'LaurentNegx2'
        y = 1./(x.^2+1);
        yni = @(xdom, n) (-1)^(n)*xdom.^(-(2*n+2)); 
        conv = 'full';
    case 'TaylorFailx'
        y = 1./(1-x);
        yni = @(xdom, n) xdom.^n; 
        conv = 'full'; 
    case 'LaurentNegx'
        y = 1./(1-x);
        yni = @(xdom, n) -xdom.^(-(n+1)); 
        conv = 'full';     
end

%% Plot and computation of all terms
figure('Units', 'normalized', 'Position', [0.01 0.01 0.95 0.95]); 
plot(x,y,'linewidth', 3)    
title(strcat('approximation of ',{' '}, simu,{' '}, 'with 0  power series terms'))
hold on, grid on; axis(pvec)
yapprox = x*0;
pause();
for n = 0:50
    switch conv
        case 'BiggerThanz0'
            if n < 8
                yn = yni(x, n);
                ptime = p1;
            else
                yn = [NaN(1,(lim-nlim)/dt) yni(x2,n) NaN(1,(lim-nlim)/dt) ];
                ptime = p2;
            end
        case 'SmallerThanZ0'
            if n < 8
                yn = yni(x, n);
                ptime = p1;
            else
                yn = [ yni(x3_1,n) NaN(1,2*nlim/dt-1) yni(x3_2,n)];
                ptime = p2;
            end
        otherwise
            yn = yni(x, n);
            if n < 8
                ptime = p1;
            else
                ptime = p2;
            end
    end
    yapprox = yapprox + yn;     % update the approximation with the latest term
    plot(x, yapprox)            % plot new approximated function
    axis(pvec)                  % as plotting resets axis, set axis to right window
    title(strcat('approximation of ',{' '} , simu, 'with ', {' '}, num2str(n), {' '},' power series terms'))
    pause(ptime)                % pause for simulation
end
plot(x, yapprox, 'r--', 'linewidth', 3)  %replot the last approx in bold red
axis(pvec)                               % as plotting resets axis, set axis to right window